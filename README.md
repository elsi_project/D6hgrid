# D6hgrid
generated integration grids on sphere with D6h symmetry.

# usage
```
real*8 :: x(xxxxx), y(xxxxx), z(xxxxx), w(xxxxx)
call d6hgridxxxxx ( x, y, z, w)
```
The output are the double precision arrays with the length of xxxxx. The x, y, and z are the arrays for the coordinates of points on the unit sphere. The w is the array of the corrspondent weight for these points.
